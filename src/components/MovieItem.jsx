import PropTypes from "prop-types";
import { useState } from 'react';


import "./MovieItem.css";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  //create a "favorite" state, default value : false

  const [favorite, setFavorite] = useState(false);

  // create a function toggleFavorite to toggle the "favorite" state

  function toggleFavorite() {
    setFavorite(!favorite); //if favorite is false, become true / if is true, become false
  }

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      {/* on click, call the function toggleFavorite */}
      <button type="button" onClick={toggleFavorite}>
        {/* change text of the button depending on favorite state */}
        {favorite? 'Remove from favorites': 'Add to favorites'}
      </button>
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
